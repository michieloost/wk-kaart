import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { MapComponent } from '../map/map.component';
import { MapContainerComponent } from '../map-container/map-container.component';

const routes: Routes = [
  {
    path: '',
    component: MapContainerComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule
  ],
  exports: [
    RouterModule,
  ],
  declarations: [],
  providers: [

  ]
})
export class AppRouterModule { }
