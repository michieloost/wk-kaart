import { Component, OnInit, Output, EventEmitter, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { LayerRepository } from '../shared/LayerRepository';
import { MapInteractionService } from '../shared/map-interaction.service';
import { ParcelService } from '../shared/parcel.service';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {

  public map;
  public layerRepo = LayerRepository;
  public selectedBasemap: string;
  public selectedParcel;
  public baseLayerGroup: any = {};
  public highlightLayer: any = {};
  public parcelData: any;
  public dataLayer: L.GeoJSON;
  public baselayerSubscription;
  public geocoderSubscription;
  public highlightFeatureSubscription;
  public parcelStylingSubscription;

  constructor(
    public mapInteractionService: MapInteractionService,
    public parcelService: ParcelService
  ) {
  }

  ngOnInit() {
    this.selectedBasemap = this.layerRepo.basemap.name;
    this.parcelData = this.parcelService.getParcelData();
  }

  ngAfterViewInit() {
    /* map */
    this.map = new L.Map('map', {
      center: new L.LatLng(52.092314, 5.12443),
      maxZoom: 24,
      zoom: 13,
      attributionControl: false,
      zoomControl: false
    });


    this.baseLayerGroup = new L.LayerGroup().addTo(this.map);
    this.highlightLayer = new L.LayerGroup().addTo(this.map);

    LayerRepository.basemap.layer.addTo(this.baseLayerGroup);

    this.subscribeToBaseLayer();
    this.subscribeToHighlightFeature();
    this.subscribeToParcelStylingSelection();
    this.drawDataLayer(null);

    this.map.fitBounds(this.dataLayer.getBounds());
    // this.map.setMinZoom(this.map.getZoom());
  }

  private subscribeToParcelStylingSelection() {
    this.parcelStylingSubscription = this.parcelService.getMaatregelSelection().subscribe((maatregelId: number) => {
      this.styleParcelsOnMaatregelSelection(maatregelId);
    });
  }

  private styleParcelsOnMaatregelSelection(maatregelId) {
    this.dataLayer.clearLayers();
    this.drawDataLayer(maatregelId);
  }

  private drawDataLayer(maatregelId) {
    this.dataLayer = L.geoJson(this.parcelData,  {
      onEachFeature: (feature, layer) => {
        layer.on('click', (e) => {
          this.mapInteractionService.setMapClick(feature);
          this.mapInteractionService.setHighlightFeature(feature.geometry);
        });
      },
      style: (feature) => {
        return this.determineParcelStyle(feature, maatregelId);
      }
    }).addTo(this.map);

  }

  private determineParcelStyle(feature, maatregelId) {
    let score;
    if (maatregelId !== null) {
      score = feature.properties.maatregelscores.find((maatregelscore) => {
        return maatregelscore.id === maatregelId;
      }).score;
    }
    let fillColor = '#828282';
    const classBoundaries = [0.2, 0.4, 0.6, 0.8];
    const nullColor = '#353535';
    const colorScale = ['#edf8e9', '#bae4b3', '#74c476', '#31a354', '#006d2c'];

    if (score === null) {
      fillColor = nullColor;
    } else if (score < classBoundaries[0]) {
      fillColor = colorScale[0];
    } else if (score >= classBoundaries[0] && score < classBoundaries[1]) {
      fillColor = colorScale[1];
    } else if (score >= classBoundaries[1] && score < classBoundaries[2]) {
      fillColor = colorScale[2];
    } else if (score >= classBoundaries[2] && score < classBoundaries[3]) {
      fillColor = colorScale[3];
    } else if (score >= classBoundaries[3]) {
      fillColor = colorScale[4];
    }

    const styleObject = {
      weight: 1,
      color: 'rgb(42, 124, 97)',
      fillColor,
      fillOpacity: 0.9
    };

    return styleObject;
  }

  private subscribeToHighlightFeature() {
    this.highlightFeatureSubscription = this.mapInteractionService.getHighLightFeature().subscribe((geometry: any) => {
      this.highlightLayer.clearLayers();
      if (geometry) {
        const layer = L.geoJSON(geometry, {
          color: 'rgb(42, 124, 97)',
        }).addTo(this.highlightLayer);
        }
      });
  }

  private subscribeToBaseLayer() {
    this.baselayerSubscription = this.mapInteractionService.getActiveBaselayer().subscribe((activeBaseLayer: any) => {
      this.baseLayerGroup.clearLayers();
      if (activeBaseLayer) {
          activeBaseLayer.layer.addTo(this.baseLayerGroup);
      }
    });
  }

  public switchBaselayer(event) {
    // this.mapInteractionService.clearActiveLayers();
    this.mapInteractionService.switchBaselayer(LayerRepository[event.value]);
  }

}
