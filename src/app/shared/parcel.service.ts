import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as maatregelen from '../../assets/maatregelen.json';
const castMaatregelen = maatregelen as any;

import * as maatregelIndicaties from '../../assets/maatregel_indicaties.json';
const castMaatregelIndicaties = maatregelIndicaties as any;

import * as parcels from '../../assets/data_v8.json';
const castParcels = parcels as any;

@Injectable()
export class ParcelService {
  public activeBaselayer = new Subject();
  public geocoderResult = new Subject();
  public mapClickEvent = new Subject();
  public highlightFeature = new Subject();
  public maatregelSelection = new Subject();
  public maatRegels = castMaatregelen;
  public maatRegelIndicaties = castMaatregelIndicaties;
  public parcels;

  constructor() {
        this.parcels = this.parseData(castParcels);
    }

    public setMaatregelSelection(maatregelId) {
        this.maatregelSelection.next(maatregelId);
    }

    public getMaatregelSelection() {
        return this.maatregelSelection.asObservable();
    }

    public getParcelData() {
        return this.parcels;
    }

    public fetchMaatregelStatistics() {
        return this.getMaatregelStatistics(this.parcels);
    }

    private getMaatregelStatistics(parcels) {
        let maatregelArray;
        if (parcels.features.length > 0) {
            maatregelArray = this.makeMaatregelArray(parcels);
        }
        maatregelArray.forEach((maatregel) => {
            maatregel.scores = parcels.features.map((feature) => {
                const mrScore = feature.properties.maatregelscores.find((maatregelscore) => {
                    return maatregelscore.id === maatregel.id;
                });
                if (mrScore !== undefined && mrScore.score !== null) {
                    return mrScore.score;
                }
            });
            const scoreArray = maatregel.scores.filter((ms) => {
                return ms !== undefined;
            });
            const sum = scoreArray.reduce((a, b) => a + b);
            maatregel.averageScore = sum / scoreArray.length;
            maatregel.maxScore = Math.max(...scoreArray);
            maatregel.minScore = Math.min(...scoreArray);
        });
        return maatregelArray;
    }

    private makeMaatregelArray(parcels) {
        return parcels.features[0].properties.maatregelscores.map((score) => {
            return {
                id: score.id,
                maatregel: score.maatregel,
                category: score.category,
                description: this.maatRegelIndicaties.find((mrI) => {
                    return mrI.id === score.id;
                }).description
            };
        });
    }


    private parseData(data) {
        data.features.forEach((row) => {
            const mappedScoresArray = row.properties.maatregelscores.map((maatregelscore) => {
                let score;
                if (maatregelscore.score === '') {
                    score = null;
                } else {
                    score = maatregelscore.score;
                }
                const newMaatregelObject = {
                    id: maatregelscore.id,
                    maatregel: maatregelscore.maatregel,
                    score: score,
                    watertrap_score: maatregelscore.watertrap_score,
                    stikstof_score: maatregelscore.stikstof_score,
                    afstand_natuur_score: maatregelscore.afstand_natuur_score,
                    afstand_storing_score: maatregelscore.afstand_scoring_score,
                    bodem_score: maatregelscore.bodem_score,
                    landschap_score: maatregelscore.landschap_score,
                    perceelgrootte_score: maatregelscore.perceelgrootte_score,
                    category: this.findMaatregelCategory(maatregelscore.id),
                    description: this.findMaatregelDescription(maatregelscore.id)
                };
                return newMaatregelObject;
            });
            row.properties.maatregelscores = mappedScoresArray;
        });
        return data;
    }

    private findMaatregelDescription(maatregelId) {
        return this.maatRegelIndicaties.find((mrI) => {
            return mrI.id === maatregelId;
        }).description;
    }

    private findMaatregelCategory(maatregelId) {
        let foundCategory;
        this.maatRegels.forEach((category) => {
            const categoryName = category.plaats;
            const maatregelen = category.maatregelen.map((maatregel) => {
                return maatregel.id;
            });
            if (maatregelen.includes(maatregelId)) {
                foundCategory = categoryName;
            }
        });
        return foundCategory;
    }
}
