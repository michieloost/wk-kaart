import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MapInteractionService {
  public activeBaselayer = new Subject();
  public geocoderResult = new Subject();
  public mapClickEvent = new Subject();
  public highlightFeature = new Subject();

  constructor(
  ) { }

  // base layer control
  public switchBaselayer(layer) {
    this.activeBaselayer.next(layer);
  }

  public getActiveBaselayer() {
    return this.activeBaselayer.asObservable();
  }

  // map click event handling
  public setMapClick(data) {
    this.mapClickEvent.next(data);
  }

  public getMapClickEvents() {
    return this.mapClickEvent.asObservable();
  }

  // highlight feature layer handling
  public setHighlightFeature(geometry) {
    this.highlightFeature.next(geometry);
  }

  public getHighLightFeature() {
    return this.highlightFeature.asObservable();
  }

  public clearHighLightFeature() {
    this.highlightFeature.next();
  }
}
