import { ConnectionBackend, Http, RequestOptions, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class ApiHttpService {

  constructor(private http: HttpClient) {
  }

  public get(url: string) {
    return this.http.get(url).toPromise()
      .then(result => {
        return result;
      })
      .catch(err => {
        console.log(err);
      });
  }
}
