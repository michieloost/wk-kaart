import * as L from 'leaflet';

const basemap = {
    name: 'basemap',
    alias: 'Kaart',
    layer: L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
        maxZoom: 20,
    })
};

const layerRepository = {
  basemap: basemap
};

export const LayerRepository = layerRepository;
