import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApiHttpService } from '../shared/api-http.service';

import { MapInteractionService } from '../shared/map-interaction.service';
import { ParcelService } from './parcel.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ApiHttpService,
    MapInteractionService,
    ParcelService
  ]
})
export class SharedModule { }
