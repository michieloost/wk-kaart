import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { MapInteractionService } from '../shared/map-interaction.service';
import { LayerRepository } from '../shared/LayerRepository';
import { ParcelService } from '../shared/parcel.service';
import { Subscription } from 'rxjs/Subscription';
import { style, state, animate, transition, trigger } from '@angular/core';

import * as maatregelIndicaties from '../../assets/maatregel_indicaties.json';
const castMaatregelIndicaties = maatregelIndicaties as any;

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger('slideIn', [
      transition(':enter', [
        style({left: -600}),
        animate(200, style({left: 0}))
      ])
    ])
  ]
})
export class SidebarComponent implements OnInit, AfterViewInit {
  public activeMaatregel: any;
  public layerRepo = LayerRepository;
  public selectedBasemap: string;
  public maatregelIndicaties = castMaatregelIndicaties;
  public chosenCategory;
  public chosenMaatregel;
  public maatregelen = [];
  public showMoreMaatregelen = false;
  public showMoreMaatregelenTop5 = false;
  public parcelViewActive = false;
  public mapClickSubscription: Subscription;
  public parcelData;

constructor(
    private mapInteractionService: MapInteractionService,
    private parcelService: ParcelService
  ) { }

  ngOnInit() {
      /* layer switch */

      this.subscribeToMapClickEvent();
  }

  ngAfterViewInit() {

  }

  public closeParcelView() {
    this.parcelData = null;
    this.parcelViewActive = false;
    this.mapInteractionService.clearHighLightFeature();
  }


  private subscribeToMapClickEvent() {
    this.mapClickSubscription = this.mapInteractionService.getMapClickEvents().subscribe((data: any) => {
      this.parcelViewActive = true;
      this.parcelData = data;
      this.parcelData.properties.maatregelscores.sort((a, b) => b.score - a.score);
    });
  }

}
