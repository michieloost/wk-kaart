import { Component } from '@angular/core';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'WK Kaart';

}
